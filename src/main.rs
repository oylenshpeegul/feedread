use chrono::{DateTime, Duration, Local};
use clap::Parser;
use futures::future;
use rss::Channel;
use std::collections::HashMap;
use std::fs;
use std::io::Cursor;
use std::path::{Path, PathBuf};
use std::str::FromStr;

#[derive(Debug, Parser)]
#[command(about, version, author)]
struct Args {
    /// Debug mode
    #[arg(short, long)]
    debug: bool,

    /// Number of days to look back
    #[arg(long, default_value_t = 14)]
    days: u8,

    /// File of download history
    #[arg(long, value_parser, default_value = "./download_history")]
    download_history: PathBuf,

    /// File of feed URLs
    #[arg(short, long, value_parser, default_value = "./feeds")]
    feeds: PathBuf,

    /// Output directory
    #[arg(
        short,
        long,
        value_parser,
        default_value = "/home/tim/Downloads/Podcasts"
    )]
    outdir: PathBuf,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
}

#[derive(Debug)]
struct Feed {
    url: String,
    name: Option<String>,
}

impl FromStr for Feed {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.splitn(2, '#');
        let url = it.next().unwrap().trim().to_string();
        let name = it.next().map(|name| name.trim().to_string());
        Ok(Feed { url, name })
    }
}

#[derive(Debug)]
struct Podcast {
    file_name: String,
    url: String,
}

// Name your user agent after your app?
const APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    if args.debug {
        dbg!(&args);
    }

    let mut download_history = get_download_history(&args.download_history)?;
    if args.debug {
        dbg!(&download_history);
    }

    let feeds = get_feeds(&args.feeds)?;
    if args.debug {
        dbg!(&feeds);
    }

    let old = Local::now() - Duration::days(args.days.into());

    let client = reqwest::Client::builder()
        .user_agent(APP_USER_AGENT)
        .build()?;

    let mut podcasts = vec![];

    for feed in feeds {
        if args.verbose > 0 {
            eprintln!("{:?}", &feed);
        }

        let channel = get_feed(&client, &feed.url).await?;
        if args.debug {
            dbg!(&channel);
        }

        for item in channel.items {
            if let Some(s) = item.pub_date {
                let dt = DateTime::parse_from_rfc2822(&s)?;
                if dt < old {
                    continue;
                }
                if let Some(e) = item.enclosure {
                    let file_name = match &feed.name {
                        Some(s) => format!("{}-{}.mp3", s, dt.format("%F")),
                        None => Path::new(&e.url)
                            .file_name()
                            .unwrap()
                            .to_str()
                            .unwrap()
                            .split_inclusive(".mp3")
                            .next()
                            .unwrap()
                            .to_string(),
                    };

                    if download_history.contains_key(&*file_name) {
                        if args.verbose > 0 {
                            println!("File {:?} downloaded already.", &file_name);
                        }
                        continue;
                    }

                    download_history.insert(file_name.clone(), dt.timestamp());
                    podcasts.push(Podcast {
                        file_name,
                        url: e.url,
                    });
                }
            }
        }
    }

    if args.debug {
        dbg!(&podcasts);
        dbg!(&download_history);
    }

    let v = podcasts.iter().map(|p| get_file(&client, p, &args.outdir));
    let responses = future::join_all(v).await;

    if args.verbose > 0 {
        eprintln!("{:?}", &responses);
    }

    let new_contents = serde_json::to_string(&download_history)?;
    fs::write(&args.download_history, new_contents)?;

    Ok(())
}

fn get_feeds(feeds: &PathBuf) -> Result<Vec<Feed>, Box<dyn std::error::Error>> {
    let contents = fs::read_to_string(feeds)?;

    let feeds = contents
        .lines()
        .map(Feed::from_str)
        .collect::<Result<Vec<_>, Box<dyn std::error::Error>>>()?;

    Ok(feeds)
}

fn get_download_history(
    download_history: &PathBuf,
) -> Result<HashMap<String, i64>, Box<dyn std::error::Error>> {
    let dh = if download_history.is_file() {
        let contents = fs::read_to_string(download_history)?;
        serde_json::from_str(&contents)?
    } else {
        HashMap::new()
    };

    Ok(dh)
}

async fn get_file(
    client: &reqwest::Client,
    podcast: &Podcast,
    outdir: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut path = outdir.to_path_buf();
    path.push(&podcast.file_name);

    let mut file = std::fs::File::create(path)?;
    let mut content = Cursor::new(client.get(&podcast.url).send().await?.bytes().await?);
    std::io::copy(&mut content, &mut file)?;

    Ok(())
}

async fn get_feed(
    client: &reqwest::Client,
    url: &str,
) -> Result<Channel, Box<dyn std::error::Error>> {
    let res = client.get(url).send().await?;
    if res.status() != 200 {
        return Err(format!("Response status {}", res.status()).into());
    }
    let content = res.bytes().await?;
    let channel = Channel::read_from(&content[..])?;
    Ok(channel)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::BufReader;

    #[tokio::test]
    async fn local_same_as_remote() {
        let file = File::open("sample.xml").unwrap();
        let local_channel = Channel::read_from(BufReader::new(file)).unwrap();

        let client = reqwest::Client::new();
        let remote_channel = get_feed(&client, "https://feedforall.com/sample.xml")
            .await
            .unwrap();

        assert_eq!(local_channel, remote_channel);
    }
}
